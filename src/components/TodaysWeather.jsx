import { Card, CardBody, CardHeader } from "@material-tailwind/react";
import React from "react";
import { TodaysForecastItem } from "./TodaysForecastItem";

export function TodaysWeather({ data }) {
    console.log(data);
    return (
        <div>
            <Card className="bg-gray-900">
                <CardHeader className="bg-black border-gray-900 border-4 text-white">
                    <h1 className="text-[1.5rem] py-4 px-8">
                        Today's Forecast
                    </h1>
                </CardHeader>
                <CardBody>
                    <div className="grid grid-cols-2 gap-4">
                        <TodaysForecastItem
                            parameter={"maxwind_kph"}
                            data={data}
                        ></TodaysForecastItem>
                        <TodaysForecastItem
                            parameter={"daily_chance_of_rain"}
                            data={data}
                        ></TodaysForecastItem>
                        <TodaysForecastItem
                            parameter={"totalprecip_mm"}
                            data={data}
                        ></TodaysForecastItem>
                        <TodaysForecastItem
                            parameter={"uv"}
                            data={data}
                        ></TodaysForecastItem>
                        <TodaysForecastItem
                            parameter={"sunrise"}
                            data={data}
                        ></TodaysForecastItem>
                        <TodaysForecastItem
                            parameter={"sunset"}
                            data={data}
                        ></TodaysForecastItem>
                        <TodaysForecastItem
                            parameter={"is_sun_up"}
                            data={data}
                        ></TodaysForecastItem>
                    </div>
                </CardBody>
            </Card>
        </div>
    );
}
