import React from "react";

import cities from "./../images/icons/cities.jpeg";
import maps from "./../images/icons/maps.jpeg";
import settings from "./../images/icons/settings.png";
import weather from "./../images/icons/weather.png";

const imageMap = {
    cities: cities,
    maps: maps,
    settings: settings,
    weather: weather,
};

export function LeftNavItem({ title, iconURL }) {
    console.log(title + " : " + iconURL);
    return (
        <div className="flex flex-col px-4 justify-center items-center">
            <div>
                <img
                    src={imageMap[iconURL]}
                    alt={title}
                    className="w-8 h-8 rounded-full"
                />
            </div>
            <div>{title}</div>
        </div>
    );
}
