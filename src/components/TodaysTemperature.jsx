import React from "react";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";

import { Swiper, SwiperSlide } from "swiper/react";
import { Scrollbar, A11y } from "swiper/modules";

import { HourlyTemperatureCard } from "./HourlyTemperatureCard";

export function TodaysTemperature({ hourlyData }) {
    return (
        <div className="flex justify-center items-center gap-4">
            <Swiper
                id="course-swiper"
                modules={[Scrollbar, A11y]}
                spaceBetween={0}
                slidesPerView={3}
                scrollbar={{ draggable: true }}
                onSlideChange={() => console.log("slide change")}
                onSwiper={(swiper) => console.log(swiper)}
                className="grid grid-cols-3  gap-10  px-20 justify-center items-center"
            >
                {hourlyData?.map((hourData) => (
                    <SwiperSlide
                        key={hourData["time"]}
                        className="flex  flex-col justify-center items-center text-black  pb-10  "
                    >
                        <HourlyTemperatureCard
                            time={hourData["time"].substring(10)}
                            icon={hourData["condition"]["icon"]}
                            temperature={hourData["temp_c"]}
                        />
                    </SwiperSlide>
                ))}
            </Swiper>
        </div>
    );
}
