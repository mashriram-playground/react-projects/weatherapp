import React from "react";
import { LeftNavItem } from "./LeftNavItem";

const navItems = [
    {
        id: "1",
        title: "Weather",
        iconURL: "weather",
    },
    {
        id: "2",
        title: "Cities",
        iconURL: "cities",
    },
    {
        id: "3",
        title: "Map",
        iconURL: "maps",
    },
    {
        id: "4",
        title: "Settings",
        iconURL: "settings",
    },
];

import logo from "./../images/logo.jpg";

export function LeftNav() {
    return (
        <div className="flex flex-col gap-8 justify-center items-center">
            <div className="p-4">
                <img
                    src={logo}
                    alt={"Weather App"}
                    className="w-16 h-16 rounded-full"
                />
            </div>
            {navItems.map((navItem) => (
                <LeftNavItem
                    key={navItem.id}
                    title={navItem.title}
                    iconURL={navItem.iconURL}
                />
            ))}
        </div>
    );
}
