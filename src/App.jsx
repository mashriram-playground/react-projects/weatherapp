import { HomePage } from "./pages/HomePage";

function App() {
    return (
        <div className="w-screen h-screen bg-black text-white">
            <HomePage />
        </div>
    );
}

export default App;
